# Viewer URL settings

Various settings could be set from URL hash part in order to create permalinks.

These are set after the `#` symbol of the URL, following a `key=value` format, each being separated by `&` symbol.

Example:

`https://geovisio.fr/viewer#map=19.51/48.1204522/-1.7199004&pic=890b6268-7716-4e34-ada9-69985e6c1657`

__Contents__

[[_TOC_]]


## Available parameters

### `focus`: main shown element

Switch to choose which element between map or picture should be shown wide at start. Examples:

- `focus=map`
- `focus=pic`

By default, picture is shown wide.

### `map`: map position

The map current position (if map is enabled), based on [MapLibre GL JS hash format](https://maplibre.org/maplibre-gl-js-docs/api/map/#map-parameters):

`zoom/latitude/longitude`

Example:

`map=19.51/48.1204522/-1.7199004`

This parameter is automatically updated when map is moved. If missing, whole world map is shown by default.

### `pic`: picture ID

The currently selected picture ID. Example:

`pic=890b6268-7716-4e34-ada9-69985e6c1657`

### `xyz`: picture position

The shown position in picture, following this format:

`x/y/z`

With:

- `x`: the heading in degrees (0 = North, 90 = East, 180 = South, 270 = West)
- `y`: top/bottom position in degrees (-90 = bottom, 0 = front, 90 = top)
- `z`: zoom level (0 = minimum/wide view, 100 = maximum/full zoom)

Example:

`10/25/50`


## Next steps

- Look out how you can [develop on our code](./09_Develop.md)
- Check out [STAC API compatibility notes](./05_Compatibility.md)
