# GeoVisio viewer hands-on guide

Welcome to GeoVisio __Viewer__ documentation ! It will help you through all phases of setup, run and develop on GeoVisio JS pictures viewer.

Also, if at some point you're lost or need help, you can contact us through [issues](https://gitlab.com/geovisio/web-viewer/-/issues) or by [email](mailto:panieravide@riseup.net).


__Contents__

[[_TOC_]]


## Install the viewer

### NPM

GeoVisio viewer is available on NPM as [geovisio](https://www.npmjs.com/package/geovisio) package.

```bash
npm install geovisio
```

If you want the latest version (corresponding to the `develop` git branch), you can use the `develop` NPM dist-tag:

```bash
npm install geovisio@develop
```

### Hosted version

You can rely on various providers offering hosted NPM packages, for example JSDelivr.

```html
<!-- You may use another version than 1.3.1, just change the release in URL -->
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/geovisio@1.3.1/build/index.css" />
<script src="https://cdn.jsdelivr.net/npm/geovisio@1.3.1/build/index.js"></script>
```

Or use hosted version on our GeoVisio demo instance.

```html
<link rel="stylesheet" type="text/css" href="https://geovisio.fr/viewer/lib/index.css" />
<script src="https://geovisio.fr/viewer/lib/index.js"></script>
```

### Using this repository

You can install and use GeoVisio web client based on code provided in this repository.

This library relies on __Node.js 16__, which should be installed on your computer. Then, you can build the library using these commands:

```bash
npm install
npm build
```


## Basic usage

A GeoVisio viewer can be initialized with a basic HTML `div`:

```html
<div id="viewer" style="width: 500px; height: 300px"></div>
```

Then, the div is populated with this JavaScript code:

```js
var instance = new GeoVisio.default(
	"viewer",  // Div ID
	"https://geovisio.fr/api",  // STAC API endpoint
	{ map: true }  // Viewer options
);
```

All available options are listed in [this documentation](./02_Client_usage.md).

### Change map background style

GeoVisio can be configured to use a different map background than the default one. By default, an OpenStreetMap France classic style if offered (only covers metropolitan France). Changing the style is done by passing a `style` parameter on viewer setup:

```js
var viewer = new GeoVisio.default(
	"viewer",
	"https://geovisio.fr/api",
	{
		map: {
			startWide: true,
			style: "https://yourdomain.net/path/to/your/style.json"
		}
	}
);
```

If you need to customize the received JSON style for compatibility issues, this can be done by passing an object instead of a string. Here is an example based on IGN map styling, which needs some parameter to be changed:

```js
fetch("https://wxs.ign.fr/essentiels/static/vectorTiles/styles/PLAN.IGN/standard.json")
.then(res => res.json())
.then(style => {
  // Patch tms scheme to xyz to make it compatible for Maplibre GL JS / Mapbox GL JS
  style.sources.plan_ign.scheme = 'xyz';
  style.sources.plan_ign.attribution = 'Données cartographiques : © IGN';

  var viewer = new GeoVisio.default(
    "viewer",
    "https://geovisio.fr/api",
    {
      map: {
        startWide: true,
        style
      }
    }
  );
});
```

### Use another geocoder

The map offers a search bar for easily locating places based on user text search. This is handled by [MapLibre GL Geocoder](https://github.com/maplibre/maplibre-gl-geocoder). By default, GeoVisio uses [Nominatim](https://nominatim.org/) API, which provides geocoding using OpenStreetMap data.

You can switch to using another geocoder though, we also directly offer the [Base adresse nationale](https://adresse.data.gouv.fr/) API (French authority geocoder) that you can use like this:

```js
var viewer = new GeoVisio.default(
	"viewer",
	"https://geovisio.fr/api",
	{
		map: {
			geocoder: { engine: "ban" }
		}
	}
);
```

And you can also define your own custom geocoder using these options:

```js
var myOwnGeocoder = function(config) {
	// Call your API
	// Config parameter is based on geocoderApi.forwardGeocode.config structure
	// Described here : https://github.com/maplibre/maplibre-gl-geocoder/blob/main/API.md#setgeocoderapi

	// It returns a promise resolving on a Carmen GeoJSON FeatureCollection
	// Format is described here : https://docs.mapbox.com/api/search/geocoding/#geocoding-response-object
}

var viewer = new GeoVisio.default(
	"viewer",
	"https://geovisio.fr/api",
	{
		map: {
			geocoder: { geocoderApi: {
				forwardGeocode: myOwnGeocoder
			} }
		}
	}
);
```

### Authentication against API

If the STAC API you're using needs some kind of authentication, you can pass it through Web Viewer options. Parameter `fetchOptions` allows you to set custom parameters for the [JS fetch function](https://developer.mozilla.org/en-US/docs/Web/API/fetch#parameters), like the `credentials` setting. For example:

```js
var viewer = new GeoVisio.default(
	"viewer",
	"https://your-secured-stac.fr/api",
	{
		fetchOptions: {
			credentials: "include"
		}
	}
);
```

## Next steps

You may want to:

- Dive deep in [available JS methods](./02_Usage.md)
- Discover the available [URL settings](./03_URL_settings.md)
