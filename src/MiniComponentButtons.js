import "./css/MiniComponentButtons.css";
import MinimizeIcon from "./img/minimize.svg";
import ExpandIcon from "./img/expand.svg";
import PictureIcon from "./img/picture.svg";
import MapIcon from "./img/map.svg";

/**
 * Mini component buttons is a helper to create top-right corner buttons on reduced version of map/viewer.
 * It offers a same design style for both, and handle events.
 *
 * @param {Viewer} parent The parent container
 * @private
 */
export default class MiniComponentButtons {
	constructor(parent, lang) {
		this._parent = parent;
		this._createButtonsDOM(lang);
	}

	/**
	 * Initializes the DOM
	 *
	 * @private
	 */
	_createButtonsDOM(lang) {
		// Button in bottom left corner (mini widget hidden)
		this.badgeBtn = document.createElement("button");
		this.badgeBtn.classList.add("gvs-mini-buttons-badge");
		const badgeImg = document.createElement("img");
		badgeImg.src = this._parent.isMapWide() ? PictureIcon : MapIcon;
		this._parent.addEventListener("focus-changed", () => {
			badgeImg.src = this._parent.isMapWide() ? PictureIcon : MapIcon;
			badgeImg.title = this._parent.isMapWide() ? lang.show_psv : lang.show_map;
		});
		badgeImg.alt = lang.expand;
		badgeImg.title = this._parent.isMapWide() ? lang.show_psv : lang.show_map;
		this.badgeBtn.appendChild(badgeImg);
		this.badgeBtn.addEventListener("click", () => {
			this._parent.setUnfocusedVisible(true);
		});
		this._parent.container.appendChild(this.badgeBtn);

		// Buttons in map/viewer top right corner (mini widget visible)
		this.container = document.createElement("div");
		this.container.classList.add("gvs-mini-buttons");

		this.btnMinimize = document.createElement("button");
		const btnMinimizeIcon = document.createElement("img");
		btnMinimizeIcon.src = MinimizeIcon;
		btnMinimizeIcon.alt = lang.minimize;
		btnMinimizeIcon.title = lang.minimize_help;
		this.btnMinimize.addEventListener("click", () => {
			this._parent.setUnfocusedVisible(false);
		});
		this.btnMinimize.appendChild(btnMinimizeIcon);
		this.container.appendChild(this.btnMinimize);

		this.btnExpand = document.createElement("button");
		const btnExpandIcon = document.createElement("img");
		btnExpandIcon.src = ExpandIcon;
		btnExpandIcon.alt = lang.expand;
		btnExpandIcon.title = lang.expand_help;
		this.btnExpand.appendChild(btnExpandIcon);
		this.btnExpand.addEventListener("click", () => {
			this._parent.setFocus(this._parent.isMapWide() ? "pic" : "map");
		});
		this.container.appendChild(this.btnExpand);

		this._parent.container.appendChild(this.container);
	}
}
