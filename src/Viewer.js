import "./css/Viewer.css";
import { Viewer as PSViewer } from "@photo-sphere-viewer/core";
import { EquirectangularTilesAdapter } from "@photo-sphere-viewer/equirectangular-tiles-adapter";
import { VirtualTourPlugin } from "@photo-sphere-viewer/virtual-tour-plugin";
import API from "./API";
import "./PreviousButton";
import "./NextButton";
import "./PlayButton";
import Map from "./Map";
import URLHash from "./URLHash";
import MiniComponentButtons from "./MiniComponentButtons";
import LoaderImgBase from "./img/loader_base.jpg";
import LoaderImgTile0 from "./img/loader_0.jpg";
import LoaderImgTile1 from "./img/loader_1.jpg";
import LogoDead from "./img/logo_dead.svg";
import LoaderImg from "./img/logo_anime.gif";
import TRANSLATIONS from "./translations.json";
import PACKAGE_JSON from "../package.json";

const BASE_PANORAMA = {
	baseUrl: LoaderImgBase,
	width: 1280,
	cols: 2,
	rows: 1,
	tileUrl: (col) => (col === 0 ? LoaderImgTile0 : LoaderImgTile1)
};

/**
 * Viewer is the main component of GeoVisio, showing pictures and map
 *
 * @fires picture-loaded
 * @fires picture-loading
 * @fires focus-changed
 * @fires view-rotated
 * @fires picture-tiles-loaded
 * @param {string|Node} container The DOM element to create viewer into
 * @param {string} endpoint URL to API to use (must be a [STAC API](https://github.com/radiantearth/stac-api-spec/blob/main/overview.md))
 * @param {object} [options] Viewer options
 * @param {string} [options.picId] Initial picture identifier to display
 * @param {number[]} [options.position] Initial position to go to (in [lat, lon] format)
 * @param {boolean} [options.player=true] Enable sequence player buttons (next/prev/play buttons)
 * @param {boolean} [options.hash=true] Enable URL hash settings
 * @param {string} [options.lang] Override language to use (defaults to navigator language, or English if translation not available)
 * @param {int} [options.transition=1500] Duration of transition in milliseconds
 * @param {object} [options.fetchOptions=null] Set custom options for fetch calls made against API ([same syntax as fetch options parameter](https://developer.mozilla.org/en-US/docs/Web/API/fetch#parameters))
 * @param {boolean|object} [options.map=false] Enable contextual map for locating pictures. Setting to true or passing an object enables the map. Various settings can be passed, either the ones defined here, or any of [MapLibre GL settings](https://maplibre.org/maplibre-gl-js-docs/api/map/#map-parameters)
 * @param {string} [options.map.picturesTiles] URL for fetching pictures vector tiles if map is enabled (defaults to "xyz" link advertised in STAC API landing page)
 * @param {boolean} [options.map.startWide] Show the map as main element at startup (defaults to false, viewer is wider at start)
 * @param {number} [options.map.minZoom=0] The minimum zoom level of the map (0-24).
 * @param {number} [options.map.maxZoom=24] The maximum zoom level of the map (0-24).
 * @param {object|string} [options.map.style] The map's MapLibre style. This must be an a JSON object conforming to the schema described in the [MapLibre Style Specification](https://maplibre.org/maplibre-gl-js-docs/style-spec/), or a URL to such JSON.
 *   For example, `http://path/to/my/page.html#2.59/39.26/53.07/-24.1/60`.
 *   An additional string may optionally be provided to indicate a parameter-styled hash, e.g. http://path/to/my/page.html#map=2.59/39.26/53.07/-24.1/60&foo=bar, where foo is a custom parameter and bar is an arbitrary hash distinct from the map hash.
 * @param {external:maplibre-gl.LngLatLike} [options.map.center=[0, 0]] The initial geographical centerpoint of the map. If `center` is not specified in the constructor options, MapLibre GL JS will look for it in the map's style object. If it is not specified in the style, either, it will default to `[0, 0]` Note: MapLibre GL uses longitude, latitude coordinate order (as opposed to latitude, longitude) to match GeoJSON.
 * @param {number} [options.map.zoom=0] The initial zoom level of the map. If `zoom` is not specified in the constructor options, MapLibre GL JS will look for it in the map's style object. If it is not specified in the style, either, it will default to `0`.
 * @param {external:maplibre-gl.LngLatBoundsLike} [options.map.bounds] The initial bounds of the map. If `bounds` is specified, it overrides `center` and `zoom` constructor options.
 * @param {object} [options.map.geocoder] Optional geocoder settings (can be any of [Maplibre GL Geocoder settings](https://github.com/maplibre/maplibre-gl-geocoder/blob/main/API.md#maplibregeocoder))
 */
class Viewer extends EventTarget {
	constructor(container, endpoint, options = {}){
		super();

		if(options == null) { options = {}; }
		if(options.map == null) { options.map = {}; }

		if(!options.testing) {
			// Display version in logs
			console.info(`
	 ▄▄▄▄▄▄▄ ▄▄▄▄▄▄▄ ▄▄▄▄▄▄▄ ▄▄   ▄▄ ▄▄▄ ▄▄▄▄▄▄▄ ▄▄▄ ▄▄▄▄▄▄▄
	█       █       █       █  █ █  █   █       █   █       █
	█   ▄▄▄▄█    ▄▄▄█   ▄   █  █▄█  █   █  ▄▄▄▄▄█   █   ▄   █
	█  █  ▄▄█   █▄▄▄█  █ █  █       █   █ █▄▄▄▄▄█   █  █ █  █
	█  █ █  █    ▄▄▄█  █▄█  █       █   █▄▄▄▄▄  █   █  █▄█  █
	█  █▄▄█ █   █▄▄▄█       ██     ██   █▄▄▄▄▄█ █   █       █
	█▄▄▄▄▄▄▄█▄▄▄▄▄▄▄█▄▄▄▄▄▄▄█ █▄▄▄█ █▄▄▄█▄▄▄▄▄▄▄█▄▄▄█▄▄▄▄▄▄▄█
			`);
			console.info(`📷 GeoVisio - Version ${PACKAGE_JSON.version} (${__COMMIT_HASH__})`);
			console.info("🆘 Issues can be reported at "+PACKAGE_JSON.repository.url);
		}

		// Set variables
		this._sequencePlaying = false;
		this._prevSequence = null;
		this._translations = this._getTranslations(options.lang || navigator.language || navigator.userLanguage);
		this._picturesSequences = {};

		if(!options.transition) { options.transition = 1500; }
		this._transitionDuration = options.transition;

		// Skip all init phases for more in-depth testing
		if(options.testing) { return; }

		// Init all DOM and components
		this._initContainerStructure(container);
		this._initLoader();
		try { this._initPSV(options); }
		catch(e) {
			this._dismissLoader(e);
		}

		endpoint = endpoint.replace("/api/search", "/api");
		this._api = new API(endpoint, { tiles: options?.map?.picturesTiles, fetch: options?.fetchOptions });

		// Read initial options from URL hash
		if(options.hash === true || options.hash === undefined) {
			this._hash = new URLHash(this);
			const hashOpts = this._hash._getCurrentHash();
			if(typeof options.map === "object") { options.map.hash = false; }

			// Restore picture from URL hash
			if(hashOpts.pic) {
				options.picId = this._hash._getCurrentHash().pic;
			}

			// Restore focus
			if(options.map && hashOpts.focus) {
				options.map.startWide = hashOpts.focus === "map";
			}

			// Restore viewer position
			if(hashOpts.xyz) {
				this._myVTour.addEventListener("node-changed", () => {
					const coords = this._hash.getXyzOptionsFromHashString(hashOpts.xyz);
					this.setXYZ(coords.x, coords.y, coords.z);
				}, { once: true });
			}

			// Restore map zoom/center
			if(options.map && typeof hashOpts.map === "string") {
				const mapOpts = this._hash.getMapOptionsFromHashString(hashOpts.map);
				if(mapOpts) {
					options.map = Object.assign({}, options.map, mapOpts);
				}
			}
		}

		// Call appropriate functions at start according to initial options
		const onceStuffReady = () => {
			if(options.picId) {
				this.goToPicture(options.picId).catch(e => this._dismissLoader(e));
				this.addEventListener("picture-loaded", () => this._dismissLoader(), { once: true });
			}
			else {
				this._dismissLoader();
			}

			if(options.position) {
				this.goToPosition(...options.position).catch(e => this._dismissLoader(e));
			}

			if(this._hash && this.map) {
				this._hash.bindMapEvents();
			}
		};

		this._api.onceReady()
			.then(() => {
				if(options.map) {
					options.map.transformRequest = this._api._getMapRequestTransform();
					this._initMap(options.map).then(onceStuffReady);
					this._miniButtons = new MiniComponentButtons(this, this._translations.gvs);
				}
				else {
					onceStuffReady();
				}
			})
			.catch(e => this._dismissLoader(e));
	}

	/**
	 * Creates appropriate HTML elements in container to host map + viewer
	 *
	 * @private
	 * @param {string|Element} container The container, either as HTML Element or ID of an in-document element
	 */
	_initContainerStructure(container) {
		this.container = typeof container === "string" ? document.getElementById(container) : container;

		if(this.container instanceof Element) {
			// Add classes to container
			this.container.classList.add("gvs");

			// Create PSV container
			this.psvContainer = document.createElement("div");
			this.psvContainer.classList.add("gvs-psv");

			// Create map container
			this.mapContainer = document.createElement("div");
			this.mapContainer.classList.add("gvs-map");

			// Create a loaders container
			this.loaderContainer = document.createElement("div");
			this.loaderContainer.classList.add("gvs-loader", "gvs-loader-visible");

			// Add in root container
			this.container.appendChild(this.psvContainer);
			this.container.appendChild(this.mapContainer);
			this.container.appendChild(this.loaderContainer);
		}
		else {
			throw new Error("Container is not a valid HTML element, does it exist in your page ?");
		}
	}

	/**
	 * Inits Photo Sphere Viewer component
	 *
	 * @private
	 * @param {object} options Parameters to send to photo sphere viewer
	 */
	_initPSV(options = {}) {
		this.psv = new PSViewer({
			container: this.psvContainer,
			adapter: [EquirectangularTilesAdapter, {
				showErrorTile: false,
				baseBlur: false,
			}],
			caption: "GeoVisio",
			withCredentials: options?.fetchOptions?.credentials == "include",
			requestHeaders: options.fetchOptions?.headers,
			panorama: BASE_PANORAMA,
			lang: this._translations.psv,
			loadingTxt: this._translations.psv.loading,
			minFov: 5,
			navbar:	[ "zoom" ]
				.concat(options.player === undefined || options.player ? [ "sequence-prev", "sequence-play", "sequence-next" ] : [])
				.concat([ "caption", "download", "move" ]),
			plugins: [
				[VirtualTourPlugin, {
					dataMode: "server",
					positionMode: "gps",
					renderMode: "3d",
					arrowPosition: "bottom",
					preload: true,
					rotateSpeed: false,
					transition: this._transitionDuration,
					getNode: picId => this._getNodeFromAPI(picId)
				}],
			],
		});

		this.psv._geovisio = this;
		this._myVTour = this.psv.getPlugin(VirtualTourPlugin);
		const thisViewer = this;

		// Tweak for arrow position
		this._myVTour.__positionArrows = function() {
			const is360 = thisViewer.getPictureMetadata()?.horizontalFov === 360;

			this.arrowsGroup.position.copy(this.viewer.state.direction).multiplyScalar(0.5);
			const s = is360 ? [0.5,2] : [0.8,1];
			const f = s[1] + (s[0] - s[1]) * (this.viewer.getZoomLevel() / 100);
			const y = is360 ?
				2.3 - (this.viewer.getZoomLevel() / 100) * 2
				: 1.3 - (this.viewer.getZoomLevel() / 100);
			this.arrowsGroup.position.y += this.config.arrowPosition === "bottom" ? -y : y;
			this.arrowsGroup.scale.set(f, f, f);
		}.bind(this._myVTour);

		// Custom events handlers
		this.psv.addEventListener("position-updated", ({position}) => {
			/**
			 * Event for viewer rotation
			 *
			 * @event view-rotated
			 * @type {object}
			 * @property {number} x New x position (in degrees, 0-360), corresponds to heading (0° = North, 90° = East, 180° = South, 270° = West)
			 * @property {number} y New y position (in degrees)
			 * @property {number} z New Z position (between 0 and 100)
			 */
			const event = new CustomEvent("view-rotated", this._positionToXYZ(position, this.psv.getZoomLevel()));
			this.dispatchEvent(event);

			this._onTilesStartLoading();
		});

		this.psv.addEventListener("zoom-updated", ({zoomLevel}) => {
			const event = new CustomEvent("view-rotated", this._positionToXYZ(this.psv.getPosition(), zoomLevel));
			this.dispatchEvent(event);

			this._onTilesStartLoading();
		});

		// Detect start of node changes
		const origSetPano = this.psv.setPanorama.bind(this.psv);
		this._myVTour.viewer.setPanorama = (p, c) => {
			const nodeMeta = thisViewer.getPictureMetadata();

			/**
			 * Event for picture starting to load
			 *
			 * @event picture-loading
			 * @type {object}
			 * @property {string} picId The picture unique identifier
			 * @property {number} lon Longitude (WGS84)
			 * @property {number} lat Latitude (WGS84)
			 * @property {number} x New x position (in degrees, 0-360), corresponds to heading (0° = North, 90° = East, 180° = South, 270° = West)
			 * @property {number} y New y position (in degrees)
			 * @property {number} z New z position (0-100)
			 */
			const event = new CustomEvent("picture-loading", {
				detail: {
					...thisViewer._positionToXYZ(thisViewer.psv.getPosition(), thisViewer.psv.getZoomLevel()),
					picId: nodeMeta.id,
					lon: nodeMeta.gps[0],
					lat: nodeMeta.gps[1]
				}
			});

			thisViewer.dispatchEvent(event);
			return origSetPano(p, c);
		};

		// Set rotation angle + fov on initial load / sequence change
		this.addEventListener("picture-loading", () => {
			const zoomToUse = 30;
			const nodeMeta = this.getPictureMetadata();
			const currentSequence = nodeMeta.sequence.id;
			if(!thisViewer._prevSequence || currentSequence !== this._prevSequence || nodeMeta.horizontalFov < 360) {
				this.psv.setOption("maxFov", Math.min(nodeMeta.horizontalFov * 3/4, 90));

				// Immediate change
				if(!this._prevSequence) {
					this.psv.rotate({ yaw: - nodeMeta.sphereCorrection.pan, pitch: 0 });
					this.psv.zoom(zoomToUse);
				}
				// Immediate change on load
				else if(currentSequence !== this._prevSequence) {
					this.addEventListener("picture-loaded", () => {
						this.psv.rotate({ yaw: - nodeMeta.sphereCorrection.pan, pitch: 0 });
						this.psv.zoom(zoomToUse);
					}, { once: true });
				}
				// Animated change
				else {
					this.psv.animate({ yaw: - nodeMeta.sphereCorrection.pan, pitch: 0, zoom: zoomToUse, speed: this._transitionDuration });
				}
			}
		});

		this._myVTour.addEventListener("node-changed", (e) => {
			if(e.node.id) {
				const picMeta = this.getPictureMetadata();
				this._prevSequence = picMeta.sequence.id;

				// Change download URL
				if(picMeta.panorama.hdUrl) {
					this.psv.setOption("downloadUrl", picMeta.panorama.hdUrl);
					this.psv.setOption("downloadName", e.node.id+".jpg");
				}
				else {
					this.psv.setOption("downloadUrl", null);
				}

				/**
				 * Event for picture load (low-resolution image is loaded)
				 *
				 * @event picture-loaded
				 * @type {object}
				 * @property {string} picId The picture unique identifier
				 * @property {number} lon Longitude (WGS84)
				 * @property {number} lat Latitude (WGS84)
				 * @property {number} x New x position (in degrees, 0-360), corresponds to heading (0° = North, 90° = East, 180° = South, 270° = West)
				 * @property {number} y New y position (in degrees)
				 * @property {number} z New z position (0-100)
				 */
				const event = new CustomEvent("picture-loaded", {
					detail: {
						...this._positionToXYZ(this.psv.getPosition(), this.psv.getZoomLevel()),
						picId: e.node.id,
						lon: picMeta.gps[0],
						lat: picMeta.gps[1]
					}
				});
				this.dispatchEvent(event);
			}

			this._onTilesStartLoading();

			// Update prev/next picture in sequence buttons
			if(this.psv.navbar.getButton("sequence-prev")) {
				if(this.getPictureMetadata() && this.getPictureMetadata().sequence.prevPic) {
					this.psv.navbar.getButton("sequence-prev").enable();
				}
				else {
					this.psv.navbar.getButton("sequence-prev").disable();
				}
			}

			if(this.psv.navbar.getButton("sequence-next")) {
				if(this.getPictureMetadata() && this.getPictureMetadata().sequence.nextPic) {
					this.psv.navbar.getButton("sequence-next").enable();
				}
				else {
					this.psv.navbar.getButton("sequence-next").disable();
				}
			}
		});
	}

	/**
	 * Inits MapLibre GL component
	 *
	 * @private
	 * @param {object} mapOptions Parameters to send to MapLibre
	 * @returns {Promise} Resolves when map is ready
	 */
	_initMap(mapOptions = {}) {
		return new Promise(resolve => {
			this.map = new Map(this, mapOptions, this._translations.map);
			this.map._map.once("load", resolve);
			this.container.classList.add("gvs-has-map");

			if(typeof mapOptions === "object" && mapOptions.startWide) {
				this.setFocus("map", true);
			}
			else {
				this.setFocus("pic", true);
			}
		});
	}

	/**
	 * Inits overlay loader component
	 *
	 * @private
	 */
	_initLoader() {
		// Logo
		const logo = document.createElement("img");
		logo.src = LoaderImg;
		logo.title = this._translations.map.loading;
		this.loaderContainer.appendChild(logo);

		// Label
		const labelWrapper = document.createElement("div");
		const label = document.createElement("span");
		const nextLabel = () => (
			this._translations.gvs.loading_labels[
				Math.floor(Math.random() * this._translations.gvs.loading_labels.length)
			]
		);
		label.innerHTML = nextLabel();
		this._loaderLabelChanger = null;
		const nextLabelFct = () => setTimeout(() => {
			label.innerHTML = nextLabel();
			this._loaderLabelChanger = nextLabelFct();
		}, 500 + Math.random() * 2000);
		this._loaderLabelChanger = nextLabelFct();
		labelWrapper.appendChild(label);

		// Blinking dots
		for(let i=0; i < 3; i++) {
			const dot = document.createElement("span");
			dot.classList.add("gvs-loader-dot");
			dot.appendChild(document.createTextNode("."));
			labelWrapper.appendChild(dot);
		}

		this.loaderContainer.appendChild(labelWrapper);
	}

	/**
	 * Dismiss loader, or show error
	 *
	 * @private
	 */
	_dismissLoader(err = null) {
		clearTimeout(this._loaderLabelChanger);

		if(!err) {
			this.loaderContainer.classList.remove("gvs-loader-visible");
			setTimeout(() => this.loaderContainer.remove(), 2000);
		}
		else {
			console.error(err);

			// Change content
			this.loaderContainer.children[0].src = LogoDead;
			this.loaderContainer.children[0].style.width = "200px";
			this.loaderContainer.children[1].style.fontFamily = "sans";
			this.loaderContainer.children[1].innerHTML = `${this._translations.gvs.error}<br /><small>${this._translations.gvs.error_subtitle}</small>`;

			// Throw error
			throw new Error("GeoVisio had a blocking exception");
		}
	}

	/**
	 * Calls API to retrieve a certain picture, then transforms into PSV format
	 *
	 * @private
	 * @param {string} picId The picture UUID
	 * @returns {Promise} Resolves on PSV node metadata
	 */
	_getNodeFromAPI(picId) {
		if(!picId) { return; }

		return fetch(this._api.getPictureMetadataUrl(picId, this._picturesSequences[picId]), this._api._getFetchOptions())
			.then(res => res.json())
			.then(metadata => {
				if(metadata.features) { metadata = metadata.features.pop(); }
				if(!metadata) { throw new Error("Picture with ID "+picId+" was not found"); }

				this._picturesSequences[picId] = metadata.collection;

				const isHorizontalFovDefined = metadata.properties["pers:interior_orientation"] && metadata.properties["pers:interior_orientation"]["field_of_view"] != null;
				let horizontalFov = isHorizontalFovDefined ? parseInt(metadata.properties["pers:interior_orientation"]["field_of_view"]) : 70;
				const is360 = horizontalFov === 360;
				const hdUrl = (Object.values(metadata.assets).find(a => a.roles.includes("data")) || {}).href;
				const matrix = metadata.properties["tiles:tile_matrix_sets"] ? metadata.properties["tiles:tile_matrix_sets"].geovisio : null;
				const prev = metadata.links.find(l => l.rel === "prev" && l.type === "application/geo+json");
				const next = metadata.links.find(l => l.rel === "next" && l.type === "application/geo+json");
				const baseUrlWebp = Object.values(metadata.assets).find(a => a.roles.includes("visual") && a.type === "image/webp");
				const baseUrlJpeg = Object.values(metadata.assets).find(a => a.roles.includes("visual") && a.type === "image/jpeg");
				const tileUrl = metadata?.asset_templates?.tiles_webp || metadata?.asset_templates?.tiles;

				if(prev) { this._picturesSequences[prev.id] = metadata.collection; }
				if(next) { this._picturesSequences[next.id] = metadata.collection; }

				const res = {
					id: metadata.id,
					caption: this._getNodeCaption(metadata).outerHTML,
					panorama: is360 ? {
						baseUrl: (baseUrlWebp || baseUrlJpeg).href,
						hdUrl,
						cols: matrix && matrix.tileMatrix[0].matrixWidth,
						rows: matrix && matrix.tileMatrix[0].matrixHeight,
						width: matrix && (matrix.tileMatrix[0].matrixWidth * matrix.tileMatrix[0].tileWidth),
						tileUrl: matrix && ((col, row) => tileUrl.href.replace(/\{TileCol\}/g, col).replace(/\{TileRow\}/g, row))
					} : {
						// Flat pictures are shown only using a cropped base panorama
						baseUrl: hdUrl,
						hdUrl,
						basePanoData: (img) => {
							if(img.width < img.height && !isHorizontalFovDefined) {
								horizontalFov = 35;
							}
							const verticalFov = horizontalFov * img.height / img.width;
							const panoWidth = img.width * 360 / horizontalFov;
							const panoHeight = img.height * 180 / verticalFov;

							return {
								fullWidth: panoWidth,
								fullHeight: panoHeight,
								croppedWidth: img.width,
								croppedHeight: img.height,
								croppedX: (panoWidth - img.width) / 2,
								croppedY: (panoHeight - img.height) / 2,
								poseHeading: metadata.properties["view:azimuth"] || 0
							};
						},
						// This is only to mock loading of tiles (which are not available for flat pictures)
						cols: 2, rows: 1, width: 2, tileUrl: () => ""
					},
					links: metadata.links
						.filter(l => ["next", "prev"].includes(l.rel) && l.type === "application/geo+json")
						.map(l => ({nodeId: l.id, gps: l.geometry.coordinates})),
					gps: metadata.geometry.coordinates,
					sequence: {
						id: metadata.collection,
						nextPic: next ? next.id : undefined,
						prevPic: prev ? prev.id : undefined
					},
					sphereCorrection: metadata.properties["view:azimuth"] ? {
						pan: - metadata.properties["view:azimuth"] * (Math.PI / 180)
					} : { pan: 0 },
					horizontalFov
				};

				return res;
			});
	}

	/**
	 * Generates the navbar caption based on a single picture metadata
	 *
	 * @private
	 * @param {object} metadata The picture metadata
	 * @returns {Element} The HTML element with all metadata to display
	 */
	_getNodeCaption(metadata) {
		const dom = document.createElement("div");
		dom.classList.add("gvs-caption");
		const entries = [];

		// Timestamp
		if(metadata?.properties?.datetime) {
			entries.push({ classname: "datetime", value: new Date(metadata.properties.datetime).toLocaleDateString(undefined, { year: "numeric", month: "long", day: "numeric" }) });
		}

		// Producer
		if(metadata?.providers) {
			const producerRole = metadata?.providers?.filter(el => el?.roles?.includes("producer")).pop();
			if(producerRole) {
				entries.push({ classname: "producer", value: producerRole.name });
			}
		}

		// Generate HTML element
		entries.forEach((e, i) => {
			if(i > 0) {
				dom.appendChild(document.createTextNode(" - "));
			}

			const eDom = document.createElement("span");
			eDom.classList.add(`gvs-caption-${e.classname}`);
			eDom.appendChild(document.createTextNode(e.value));
			dom.appendChild(eDom);
		});

		return dom;
	}

	/**
	 * Converts result from getPosition or position-updated event into x/y/z coordinates
	 *
	 * @private
	 * @param {object} pos pitch/yaw as given by PSV
	 * @param {number} zoom zoom as given by PSV
	 * @returns {object} Coordinates as x/y in degrees and zoom as given by PSV
	 */
	_positionToXYZ(pos, zoom = undefined) {
		const res = {
			x: pos.yaw * (180/Math.PI),
			y: pos.pitch * (180/Math.PI)
		};

		if(zoom) { res.z = zoom; }
		return res;
	}

	/**
	 * Converts x/y/z coordinates into PSV position (lat/lon/zoom)
	 *
	 * @private
	 * @param {number} x The X coordinate (in degrees)
	 * @param {number} y The Y coordinate (in degrees)
	 * @param {number} z The zoom level (0-100)
	 * @returns {object} Position coordinates as yaw/pitch/zoom
	 */
	_xyzToPosition(x, y, z) {
		return {
			yaw: x / (180/Math.PI),
			pitch: y / (180/Math.PI),
			zoom: z
		};
	}

	/**
	 * Get text labels translations in given language
	 *
	 * @private
	 * @param {string} lang The language code (fr, en)
	 * @returns {object} Translations in given language, with fallback to english
	 */
	_getTranslations(lang = "") {
		// Lang exists -> send it
		if(TRANSLATIONS[lang]) {
			return TRANSLATIONS[lang];
		}

		// Look for primary lang
		if(lang.includes("-")) {
			const primaryLang = lang.split("-")[0];
			if(TRANSLATIONS[primaryLang]) { return TRANSLATIONS[primaryLang]; }
		}

		if(lang.includes("_")) {
			const primaryLang = lang.split("_")[0];
			if(TRANSLATIONS[primaryLang]) { return TRANSLATIONS[primaryLang]; }
		}

		// Else, fallbacks to English
		return TRANSLATIONS.en;
	}

	/**
	 * Event handler for loading a new range of tiles
	 *
	 * @private
	 */
	_onTilesStartLoading() {
		if(this._tilesQueueTimer) {
			clearInterval(this._tilesQueueTimer);
			delete this._tilesQueueTimer;
		}
		this._tilesQueueTimer = setInterval(() => {
			if(Object.keys(this.psv.adapter.queue.tasks).length === 0) {
				if(this._myVTour.state.currentNode) {
					/**
					 * Event launched when all visible tiles of a picture are loaded
					 *
					 * @event picture-tiles-loaded
					 * @type {object}
					 * @property {string} picId The picture unique identifier
					 */
					const event = new Event("picture-tiles-loaded", { picId: this._myVTour.state.currentNode.id });
					this.dispatchEvent(event);
				}
				clearInterval(this._tilesQueueTimer);
				delete this._tilesQueueTimer;
			}
		}, 100);
	}

	/**
	 * Click handler for next/prev navbar buttons
	 *
	 * @param {('next'|'prev')} type Set if it's next or previous button
	 * @private
	 */
	_onNextPrevPicClick(type) {
		if(this.getPictureMetadata() && this.getPictureMetadata().sequence[type+"Pic"]) {
			// Actually change current picture
			if(type === "prev") {
				this.goToPrevPicture();
			}
			else if(type === "next") {
				this.goToNextPicture();
			}
		}
	}

	/**
	 * Get 2D position of sphere currently shown to user
	 *
	 * @returns {object} Position in format { x: heading in degrees (0° = North, 90° = East, 180° = South, 270° = West), y: top/bottom position in degrees (-90° = bottom, 0° = front, 90° = top) }
	 */
	getXY() {
		return this._positionToXYZ(this.psv.getPosition());
	}

	/**
	 * Get 3D position of sphere currently shown to user
	 *
	 * @returns {object} Position in format { x: heading in degrees (0° = North, 90° = East, 180° = South, 270° = West), y: top/bottom position in degrees (-90° = bottom, 0° = front, 90° = top), z: zoom (0 = wide, 100 = zoomed in) }
	 */
	getXYZ() {
		return this._positionToXYZ(this.psv.getPosition(), this.psv.getZoomLevel());
	}

	/**
	 * Access currently shown picture metadata
	 *
	 * @returns {object} Picture metadata
	 */
	getPictureMetadata() {
		return this._myVTour.state.currentNode ? Object.assign({}, this._myVTour.state.currentNode) : null;
	}

	/**
	 * Access the map object (if any)
	 * Allows you to call any of [the MapLibre GL Map functions](https://maplibre.org/maplibre-gl-js-docs/api/map/) for advanced map management
	 *
	 * @returns {null|external:maplibre-gl.Map} The map
	 */
	getMap() {
		return this.map ? this.map._map : null;
	}

	/**
	 * Access the photo viewer object
	 * Allows you to call any of the [Photo Sphere Viewer functions](https://photo-sphere-viewer.js.org/api/classes/core.viewer)
	 *
	 * @returns {external:@photo-sphere-viewer/core.Viewer} The photo viewer
	 */
	getPhotoViewer() {
		return this.psv;
	}

	/**
	 * Displays in viewer specified picture
	 *
	 * @param {string} picId The picture unique identifier
	 * @param {string} [seqId] The sequence ID this picture belongs to. Optional, it allows faster retrieval from API
	 */
	goToPicture(picId, seqId) {
		if(seqId) {
			this._picturesSequences[picId] = seqId;
		}
		// Actually move to wanted picture
		return this._myVTour.setCurrentNode(picId);
	}

	/**
	 * Goes continuously to next picture in sequence as long as possible
	 */
	playSequence() {
		this._sequencePlaying = true;

		/**
		 * Event for sequence starting to play
		 *
		 * @event sequence-playing
		 * @type {object}
		 */
		const event = new Event("sequence-playing");
		this.dispatchEvent(event);

		const nextPicturePlay = () => {
			if(this._sequencePlaying) {
				this.addEventListener("picture-loaded", () => {
					this._playTimer = setTimeout(() => {
						nextPicturePlay();
					}, 1500);
				}, { once: true });

				try {
					this.goToNextPicture();
				}
				catch(e) {
					this.stopSequence();
				}
			}
		};

		// Stop playing if user clicks on image
		this.psv.addEventListener("click", () => {
			this.stopSequence();
		});

		nextPicturePlay();
	}

	/**
	 * Stops playing current sequence
	 */
	stopSequence() {
		this._sequencePlaying = false;

		if(this._playTimer) {
			clearTimeout(this._playTimer);
			delete this._playTimer;
		}

		/**
		 * Event for sequence stopped playing
		 *
		 * @event sequence-stopped
		 * @type {object}
		 */
		const event = new Event("sequence-stopped");
		this.dispatchEvent(event);
	}

	/**
	 * Is there any sequence being played right now ?
	 *
	 * @returns {boolean} True if sequence is playing
	 */
	isSequencePlaying() {
		return this._sequencePlaying;
	}

	/**
	 * Displays next picture in current sequence (if any)
	 */
	goToNextPicture() {
		if(!this.getPictureMetadata()) {
			throw new Error("No picture currently selected");
		}

		const next = this.getPictureMetadata().sequence.nextPic;
		if(next) {
			this.goToPicture(next);
		}
		else {
			throw new Error("No next picture available");
		}
	}

	/**
	 * Displays previous picture in current sequence (if any)
	 */
	goToPrevPicture() {
		if(!this.getPictureMetadata()) {
			throw new Error("No picture currently selected");
		}

		const prev = this.getPictureMetadata().sequence.prevPic;
		if(prev) {
			this.goToPicture(prev);
		}
		else {
			throw new Error("No previous picture available");
		}
	}

	/**
	 * Displays in viewer a picture near to given coordinates
	 *
	 * @param {number} lat Latitude (WGS84)
	 * @param {number} lon Longitude (WGS84)
	 * @returns {Promise} Resolves on picture ID if picture found, otherwise rejects
	 */
	goToPosition(lat, lon) {
		return fetch(this._api.getPicturesAroundCoordinatesUrl(lat, lon), this._api._getFetchOptions())
			.then(res => res.json())
			.then(res => {
				if(res.features.length > 0) {
					this.goToPicture(res.features[0].id);
					return res.features[0].id;
				}
				else {
					return Promise.reject(new Error("No picture found nearby given coordinates"));
				}
			});
	}

	/**
	 * Is the map shown as main element instead of viewer (wide map mode) ?
	 *
	 * @returns {boolean} True if map is wider than viewer
	 */
	isMapWide() {
		if(!this.map) { throw new Error("Map is not enabled"); }
		return this.container.classList.contains("gvs-focus-map");
	}

	/**
	 * Force refresh of GeoVisio vector tiles on the map.
	 */
	reloadVectorTiles() {
		if(!this.map) { throw new Error("Map is not enabled"); }
		this.map._map.getSource("geovisio").setTiles([ this._api.getPicturesTilesUrl() ]);
	}

	/**
	 * Change the viewer focus (either on picture or map)
	 *
	 * @param {string} focus The object to focus on (map, pic)
	 * @param {boolean} [skipEvent=false] True to not send focus-changed event
	 */
	setFocus(focus, skipEvent = false) {
		if(!this.map) { throw new Error("Map is not enabled"); }

		if(focus === "map") {
			this.container.classList.add("gvs-focus-map");
		}
		else {
			this.container.classList.remove("gvs-focus-map");
		}

		this.map._map.resize();
		this.psv.autoSize();

		if(!skipEvent) {
			/**
			 * Event for focus change (either map or picture is shown wide)
			 *
			 * @event focus-changed
			 * @type {object}
			 * @property {string} focus Component now focused on (map, pic)
			 */
			const event = new CustomEvent("focus-changed", { focus });
			this.dispatchEvent(event);
		}
	}

	/**
	 * Change the visibility of reduced component (picture or map)
	 *
	 * @param {boolean} visible True to make reduced component visible
	 */
	setUnfocusedVisible(visible) {
		if(!this.map) { throw new Error("Map is not enabled"); }

		if(visible) {
			this.container.classList.remove("gvs-unfocused-hidden");
		}
		else {
			this.container.classList.add("gvs-unfocused-hidden");
		}

		this.map._map.resize();
		this.psv.autoSize();
	}

	/**
	 * Change the shown position in picture
	 *
	 * @param {number} x X position (in degrees)
	 * @param {number} y Y position (in degrees)
	 * @param {number} z Z position (0-100)
	 */
	setXYZ(x, y, z) {
		const coords = this._xyzToPosition(x, y, z);
		this.psv.rotate({ yaw: coords.yaw, pitch: coords.pitch });
		this.psv.zoom(coords.zoom);
	}
}

export default Viewer;
