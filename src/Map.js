import "./css/Map.css";
import "maplibre-gl/dist/maplibre-gl.css";
// DO NOT REMOVE THE "!": bundled builds breaks otherwise !!!
import maplibregl from "!maplibre-gl";
import maplibreglWorker from "maplibre-gl/dist/maplibre-gl-csp-worker";
import MarkerSVG from "./img/marker.svg";
import MaplibreGeocoder from "@maplibre/maplibre-gl-geocoder";
import "@maplibre/maplibre-gl-geocoder/dist/maplibre-gl-geocoder.css";
import LoaderImg from "./img/logo_anime.gif";

maplibregl.workerClass = maplibreglWorker;

/**
 * @summary Map showing photo location
 * @private
 */
export default class Map {
	/**
	 * @param {Viewer} viewer The viewer
	 * @param {object} [options] Optional settings (can be any of [MapLibre GL settings](https://maplibre.org/maplibre-gl-js-docs/api/map/#map-parameters))
	 * @param {object} [options.geocoder] Optional geocoder settings (can be any of [Maplibre GL Geocoder settings](https://github.com/maplibre/maplibre-gl-geocoder/blob/main/API.md#maplibregeocoder))
	 * @param {string} [options.geocoder.engine] Use a particular geocoder engine (nominatim, ban)
	 * @param {object} [lang] The translated labels to use
	 */
	constructor(viewer, options = {}, lang = {}) {
		this.viewer = viewer;
		this._lang = lang;

		// Create map
		this._map = new maplibregl.Map({
			container: this.viewer.mapContainer,
			style: "https://tile-vect.openstreetmap.fr/styles/basic/style.json",
			center: [0,0],
			zoom: 0,
			maxZoom: 24,
			...options
		});

		this._map.addControl(new maplibregl.NavigationControl(), "top-right");

		// Retrieve data area (if no center/zoom options has been defined)
		if((!options.center || options.center === [0,0]) && (!options.zoom || options.zoom === 0)) {
			this.viewer._api.onceReady().then(() => {
				const bbox = this.viewer._api.getDataBbox();
				if(bbox) {
					if(this._map.loaded()) { this._map.fitBounds(bbox, { "animate": false }); }
					else { this._map.on("load", () => this._map.fitBounds(bbox, { "animate": false })); }
				}
			});
		}

		// Widgets and markers
		this._picMarker = this._getPictureMarker();
		this._thumbGif = document.createElement("img");
		this._thumbGif.src = LoaderImg;
		this._thumbGif.alt = this._lang.loading;
		this._thumbGif.title = this._lang.loading;
		this._thumbGif.classList.add("gvs-map-thumb", "gvs-map-thumb-loader");
		this._map.on("load", () => {
			this._createPicturesTilesLayer();
			this._initGeocoderWidget(options.geocoder);
			this._listenToViewerEvents();
			this._map.resize();
		});

		// Cache for pictures and sequences thumbnails
		this._picThumbUrl = {};
		this._seqThumbUrl = {};
		this._seqThumbPicId = {};
	}

	/**
	 * Create pictures/sequences vector tiles layer
	 *
	 * @private
	 */
	_createPicturesTilesLayer() {
		this._map.addSource("geovisio", {
			"type": "vector",
			"tiles": [ this.viewer._api.getPicturesTilesUrl() ],
			"minzoom": 0,
			"maxzoom": 14
		});

		this._map.addLayer({
			"id": "sequences",
			"type": "line",
			"source": "geovisio",
			"source-layer": "sequences",
			...this._getSequencesLayerStyleProperties()
		});

		this._map.addLayer({
			"id": "pictures",
			"type": "circle",
			"source": "geovisio",
			"source-layer": "pictures",
			...this._getPicturesLayerStyleProperties()
		});

		// Map interaction events (pointer cursor, click)
		this._picPopup = new maplibregl.Popup({
			closeButton: false,
			closeOnClick: this.viewer?.container?.innerWidth >= 576,
			offset: 3
		});
		this._picPopup.on("close", () => { delete this._picPopup._picId; });

		this.viewer.addEventListener("picture-loaded", e => {
			if(this.viewer?.container?.offsetWidth < 576 && e.detail.picId == this._picPopup._picId) {
				this._picPopup.remove();
			}
		});

		this._map.on("mouseenter", "pictures", e => {
			this._map.getCanvas().style.cursor = "pointer";
			this._attachPreviewToPictures(e, "pictures");
		});

		this._map.on("mouseleave", "pictures", () => {
			this._map.getCanvas().style.cursor = "";
			this._picPopup.remove();
		});

		this._map.on("click", "pictures", e => {
			e.preventDefault();
			// Get first feature
			const f = e.features.pop();
			if(f) {
				// Look for a potential sequence ID
				let seqId = null;
				try {
					if(f.properties.sequences) {
						if(!Array.isArray(f.properties.sequences)) { f.properties.sequences = JSON.parse(f.properties.sequences); }
						seqId = f.properties.sequences.pop();
					}
				}
				catch(e) {
					console.log("Sequence ID is not available in vector tiles for picture "+f.properties.id);
				}
				this.viewer.goToPicture(f.properties.id, seqId);
			}
		});

		this._map.on("mouseenter", "sequences", e => {
			if(this._map.getZoom() <= 15) {
				this._map.getCanvas().style.cursor = "pointer";
				if(e.features[0].properties.id) {
					this._attachPreviewToPictures(e, "sequences");
				}
			}
		});

		this._map.on("mouseleave", "sequences", () => {
			this._map.getCanvas().style.cursor = "";
			this._picPopup.remove();
		});

		this._map.on("click", "sequences", e => {
			e.preventDefault();
			if(e.features[0].properties.id && this._map.getZoom() <= 15) {
				this._getPictureIdForSequence(e.features[0].properties.id)
					.then(picId => {
						if(picId) {
							this.viewer.goToPicture(picId);
						}
					});
			}
		});

		this._map.on("click", (e) => {
			if(e.defaultPrevented === false) {
				this._picPopup.remove();
			}
		});
	}

	/**
	 * MapLibre paint/layout properties for pictures layer
	 * This is useful when selected picture changes to allow partial update
	 *
	 * @returns {object} Paint/layout properties
	 * @private
	 */
	_getPicturesLayerStyleProperties() {
		return {
			"paint": {
				"circle-radius": ["interpolate", ["linear"], ["zoom"], 14, 3, 17, 8, 22, 12],
				"circle-color": ["case",
					["==", ["get", "hidden"], true], "#34495E",
					"#FF6F00"
				],
				"circle-opacity": ["interpolate", ["linear"], ["zoom"], 14, 0, 15, 1],
				"circle-stroke-color": "#ffffff",
				"circle-stroke-width": ["interpolate", ["linear"], ["zoom"], 17, 0, 20, 2],
			},
			"layout": {}
		};
	}

	/**
	 * MapLibre paint/layout properties for sequences layer
	 *
	 * @returns {object} Paint/layout properties
	 * @private
	 */
	_getSequencesLayerStyleProperties() {
		return {
			"paint": {
				"line-width": ["interpolate", ["linear"], ["zoom"], 0, 0.5, 10, 2, 14, 4, 16, 5, 22, 3],
				"line-color": ["case",
					["==", ["get", "hidden"], true], "#34495E",
					"#FF6F00"
				]
			},
			"layout": {
				"line-cap": "square"
			}
		};
	}

	/**
	 * Creates the geocoder search bar
	 * @private
	 * @param {object} options The Geocoder options
	 */
	_initGeocoderWidget(options = {}) {
		const engines = { "ban": Map.ForwardGeocodingBAN, "nominatim": Map.ForwardGeocodingNominatim };
		const engine = options.engine || "nominatim";

		this.geocoder = new MaplibreGeocoder(
			{
				forwardGeocode: engines[engine],
				...options.geocoderApi
			},
			{
				mapboxgl: maplibregl,
				marker: false,
				showResultMarkers: false,
				showResultsWhileTyping: true,
				...options
			}
		);
		this._map.addControl(this.geocoder, "top-left");
	}


	/**
	 * Attach a preview popup to a single picture.
	 * This is a mouseenter over pictures event handler
	 *
	 * @param {object} e Event data
	 * @param {string} from Calling layer name
	 * @private
	 */
	_attachPreviewToPictures(e, from) {
		let f = e.features[0];

		let coordinates = from === "pictures" ? f.geometry.coordinates.slice() : e.lngLat;
		while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
			coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
		}

		// Display thumbnail
		this._picPopup
			.setLngLat(coordinates)
			.setDOMContent(this._thumbGif)
			.addTo(this._map);

		this._picPopup._loading = f.properties.id;
		this._picPopup._picId = f.properties.id;

		const p = from === "pictures" ?
			this._getPictureThumbURL(f.properties.id)
			: this._getSequenceThumbURL(f.properties.id);

		p.then(thumbUrl => {
			if(this._picPopup._loading === f.properties.id) {
				delete this._picPopup._loading;

				if(thumbUrl) {
					let content = document.createElement("img");
					content.classList.add("gvs-map-thumb");
					content.alt = this._lang.thumbnail;
					let img = new Image();
					img.src = thumbUrl;

					img.addEventListener("load", () => {
						if(f.properties.hidden) {
							content.children[0].src = img.src;
						}
						else {
							content.src = img.src;
						}
						this._picPopup.setDOMContent(content);
					});

					if(f.properties.hidden) {
						const legend = document.createElement("div");
						legend.classList.add("gvs-map-thumb-legend");
						legend.appendChild(document.createTextNode(this._lang.not_public));
						const container = document.createElement("div");
						container.appendChild(content);
						container.appendChild(legend);
						content = container;
					}
				}
				else {
					this._picPopup.setHTML(`<i>${this._lang.no_thumbnail}</i>`);
				}
			}
		});
	}

	/**
	 * Get picture thumbnail URL for a given picture ID.
	 * It handles a client-side cache based on raw API responses.
	 *
	 * @param {string} picId The picture ID
	 * @returns {Promise} Promise resolving on picture thumbnail URL, or null on timeout
	 * @private
	 */
	_getPictureThumbURL(picId, seqId) {
		let res = null;

		if(picId) {
			if(this._picThumbUrl[picId] !== undefined) {
				res = typeof this._picThumbUrl[picId] === "string" ? Promise.resolve(this._picThumbUrl[picId]) : this._picThumbUrl[picId];
			}
			else {
				this._picThumbUrl[picId] = this.viewer._api.getPictureThumbnailURL(picId, seqId).then(url => {
					if(url) {
						this._picThumbUrl[picId] = url;
						return url;
					}
					else {
						this._picThumbUrl[picId] = null;
						return null;
					}
				})
					.catch(() => {
						this._picThumbUrl[picId] = null;
					});
				res = this._picThumbUrl[picId];
			}
		}

		return res;
	}

	/**
	 * Get picture thumbnail URL for a given sequence ID
	 * It handles a client-side cache based on raw API responses.
	 *
	 * @param {string} seqId The sequence ID
	 * @returns {Promise} Promise resolving on picture thumbnail URL, or null on timeout
	 * @private
	 */
	_getSequenceThumbURL(seqId) {
		let res = null;

		if(seqId) {
			if(this._seqThumbUrl[seqId] !== undefined) {
				res = typeof this._seqThumbUrl[seqId] === "string" ? Promise.resolve(this._seqThumbUrl[seqId]) : this._seqThumbUrl[seqId];
			}
			else if(typeof this._seqThumbPicId[seqId] === "string") {
				return this._getPictureThumbURL(this._seqThumbPicId[seqId], seqId).then(url => {
					if(url) {
						this._seqThumbUrl[seqId] = url;
						return url;
					}
					else {
						this._seqThumbUrl[seqId] = null;
						return null;
					}
				});
			}
			else {
				this._seqThumbUrl[seqId] = this.viewer._api.getPictureThumbnailURLForSequence(seqId).then(url => {
					if(url) {
						this._seqThumbUrl[seqId] = url;
						return url;
					}
					else {
						this._seqThumbUrl[seqId] = null;
						return null;
					}
				})
					.catch(() => {
						this._seqThumbUrl[seqId] = null;
					});
				res = this._seqThumbUrl[seqId];
			}
		}

		return res;
	}

	/**
	 * Get a picture ID for a given sequence
	 *
	 * @param {string} seqId The sequence ID
	 * @returns {Promise} Promise resolving on picture ID, or null on timeout
	 * @private
	 */
	_getPictureIdForSequence(seqId) {
		let res;
		if(this._seqThumbPicId[seqId] !== undefined) {
			if(typeof this._seqThumbPicId[seqId] === "string") {
				res = Promise.resolve(this._seqThumbPicId[seqId]);
			}
			else if(this._seqThumbPicId[seqId] === null) {
				res = Promise.resolve(null);
			}
			else {
				res = this._seqThumbPicId[seqId];
			}
		}
		else {
			this._seqThumbPicId[seqId] = this.viewer._api.getExamplePictureMetadataForSequence(seqId).then(picMeta => {
				if(picMeta && picMeta.id) {
					this._seqThumbPicId[seqId] = picMeta.id;
					return picMeta.id;
				}
				else {
					this._seqThumbPicId[seqId] = null;
					return null;
				}
			})
				.catch(() => {
					this._seqThumbPicId[seqId] = null;
					return null;
				});
			this._seqThumbUrl[seqId] = this._seqThumbPicId[seqId];
			res = this._seqThumbPicId[seqId];
		}

		return res;
	}

	/**
	 * Create a ready-to-use picture marker
	 *
	 * @returns {maplibregl.Marker} The generated marker
	 * @private
	 */
	_getPictureMarker() {
		const img = document.createElement("img");
		img.src = MarkerSVG;
		return new maplibregl.Marker({
			element: img
		});
	}

	/**
	 * Start listening to picture changes in PSV
	 *
	 * @private
	 */
	_listenToViewerEvents() {
		// Switched picture
		this.viewer.addEventListener("picture-loading", (e) => {
			// Show marker corresponding to selection
			this._picMarker
				.setLngLat([e.detail.lon, e.detail.lat])
				.setRotation(e.detail.x)
				.addTo(this._map);

			// Move map to picture coordinates
			this._map.flyTo({
				center: [e.detail.lon, e.detail.lat],
				zoom: this._map.getZoom() < 15 ? 20 : this._map.getZoom(),
				maxDuration: 2000
			});
		});

		// Picture view rotated
		this.viewer.addEventListener("view-rotated", () => {
			const x = this.viewer.psv.getPosition().yaw * (180/Math.PI);
			this._picMarker.setRotation(x);
		});
	}

	/**
	 * Transforms a set of parameters into an URL-ready string
	 * It also removes null/undefined values
	 *
	 * @param {object} params The parameters object
	 * @return {string} The URL query part
	 */
	static GeocoderParamsToURLString(params) {
		let p = {};
		Object.entries(params)
			.filter(e => e[1] !== undefined && e[1] !== null)
			.forEach(e => p[e[0]] = e[1]);

		return new URLSearchParams(p).toString();
	}

	/**
	 * Nominatim (OSM) geocoder, ready to use for our Map
	 */
	static ForwardGeocodingNominatim(config) {
		// Transform parameters into Nominatim format
		const params = {
			q: config.query,
			countrycodes: config.countries,
			limit: config.limit,
			viewbox: config.bbox,
		};

		return fetch(`https://nominatim.openstreetmap.org/search?${Map.GeocoderParamsToURLString(params)}&format=geojson&polygon_geojson=1&addressdetails=1`)
			.then(res => res.json())
			.then(res => {
				res.features = res.features.map(f => ({
					place_type: ["place"],
					place_name: f.properties.display_name,
					center: [
						f.bbox[0] +
					(f.bbox[2] - f.bbox[0]) / 2,
						f.bbox[1] +
					(f.bbox[3] - f.bbox[1]) / 2
					],
					...f
				}));
				return res;
			});
	}

	/**
	 * Base adresse nationale (FR) geocoder, ready to use for our Map
	 * @param {object} config Configuration sent by MapLibre GL Geocoder, following the geocoderApi format ( https://github.com/maplibre/maplibre-gl-geocoder/blob/main/API.md#setgeocoderapi )
	 * @returns {object} GeoJSON Feature collection in Carmen GeoJSON format
	 */
	static ForwardGeocodingBAN(config) {
		// Transform parameters into BAN format
		const params = { q: config.query, limit: config.limit };
		if(typeof config.proximity === "string") {
			const [lat, lon] = config.proximity.split(",").map(v => parseFloat(v.trim()));
			params.lat = lat;
			params.lon = lon;
		}

		const toPlaceName = p => [p.name, p.district, p.city].filter(v => v).join(", ");

		return fetch(`https://api-adresse.data.gouv.fr/search/?${Map.GeocoderParamsToURLString(params)}`)
			.then(res => res.json())
			.then(res => {
				res.features = res.features.map(f => ({
					place_type: ["place"],
					place_name: toPlaceName(f.properties),
					center: f.geometry.coordinates,
					...f
				}));
				return res;
			});
	}
}
