import Map from "../Map";
import Viewer from "../Viewer";
import fs from "fs";
import path from "path";

jest.mock("maplibre-gl", () => ({
	GeolocateControl: jest.fn(),
	Map: function(){
		return {
			addControl: jest.fn(),
			on: jest.fn(),
			resize: jest.fn(),
			addSource: jest.fn(),
			addLayer: jest.fn(),
			loaded: jest.fn(),
		};
	},
	NavigationControl: jest.fn(),
	Marker: jest.fn(),
	Popup: function(){
		return {
			on: jest.fn()
		};
	}
}));

jest.mock("@maplibre/maplibre-gl-geocoder", () => (function(opt1, opt2) { return [opt1, opt2]; }));

jest.mock("../Viewer", () => (function (mapContainer) {
	return {
		mapContainer,
		isMapWide: () => false,
		addEventListener: jest.fn(),
		_api: {
			getDataBbox: () => Promise.resolve(),
			getPicturesTilesUrl: () => "http://localhost:5000/api/map/",
			getExamplePictureMetadataForSequence: () => Promise.resolve({ id: "12345" }),
			onceReady: () => Promise.resolve(),
		},
	};
}));

describe("constructor", () => {
	it("inits", () => {
		const d = document.createElement("div");
		const p = new Viewer(d);
		const m = new Map(p);
		expect(m).toBeDefined();
	});
});

describe("_createPicturesTilesLayer", () => {
	it("works", () => {
		const d = document.createElement("div");
		const m = new Map(new Viewer(d));
		m._createPicturesTilesLayer();
		expect(m._map.addSource.mock.calls).toMatchSnapshot();
		expect(m._map.addLayer.mock.calls).toMatchSnapshot();
	});
});

describe("_initGeocoderWidget", () => {
	it("works", () => {
		const m = new Map(new Viewer());
		m._initGeocoderWidget();
		expect(m.geocoder).toMatchSnapshot();
		expect(m._map.addControl.mock.calls[1]).toEqual([m.geocoder, "top-left"]);
	});

	it("uses given parameters", () => {
		const m = new Map(new Viewer());
		m._initGeocoderWidget({ geocoderApi: { forwardGeocode: Map.ForwardGeocodingBAN } });
		expect(m.geocoder[0].forwardGeocode).toEqual(Map.ForwardGeocodingBAN);
	});

	it("handles geocoder engine param", () => {
		const m = new Map(new Viewer());
		m._initGeocoderWidget({ engine: "ban" });
		expect(m.geocoder[0].forwardGeocode).toEqual(Map.ForwardGeocodingBAN);
	});
});

describe("_getPictureThumbURL", () => {
	it("finds url against API", async () => {
		const m = new Map(new Viewer());
		m.viewer._api.getPictureThumbnailURL = () => Promise.resolve("res");
		const res = await m._getPictureThumbURL("id");
		expect(res).toBe("res");
	});

	it("hits cache if any", async () => {
		const m = new Map(new Viewer());
		m.viewer._api.getPictureThumbnailURL = () => Promise.resolve("res");
		await m._getPictureThumbURL("id");
		const res = await m._getPictureThumbURL("id");
		expect(res).toBe("res");
		expect(m._picThumbUrl["id"]).toBe("res");
	});

	it("nulls if no response from API", async () => {
		const m = new Map(new Viewer());
		m.viewer._api.getPictureThumbnailURL = () => Promise.resolve();
		const res = await m._getPictureThumbURL("id");
		expect(res).toBeNull();
	});

	it("awaits if a call is still running", async () => {
		const m = new Map(new Viewer());
		let called = 0;
		m.viewer._api.getPictureThumbnailURL = () => {
			called++;
			return new Promise(resolve => setTimeout(() => resolve("res"), 200));
		};
		m._getPictureThumbURL("id");
		const res = await m._getPictureThumbURL("id");
		expect(res).toBe("res");
		expect(called).toBe(1);
	});
});

describe("_getPictureIdForSequence", () => {
	it("finds ID", async () => {
		const m = new Map(new Viewer());
		const pm = JSON.parse(fs.readFileSync(path.join(__dirname, "data", "Viewer_pictures_1.json"))).features[0];
		m.viewer._api.getExamplePictureMetadataForSequence = () => Promise.resolve(pm);
		const res = await m._getPictureIdForSequence("id");
		expect(res).toBe("0005086d-65eb-4a90-9764-86b3661aaa77");
		expect(m._seqThumbPicId["id"]).toBe("0005086d-65eb-4a90-9764-86b3661aaa77");
	});

	it("nulls if no thumb", async () => {
		const m = new Map(new Viewer());
		m.viewer._api.getExamplePictureMetadataForSequence = () => Promise.resolve({});
		const res = await m._getPictureIdForSequence("id");
		expect(res).toBeNull();
		expect(m._seqThumbPicId["id"]).toBeNull();
	});

	it("hits cache if any", async () => {
		const m = new Map(new Viewer());
		const pm = JSON.parse(fs.readFileSync(path.join(__dirname, "data", "Viewer_pictures_1.json"))).features[0];
		m.viewer._api.getExamplePictureMetadataForSequence = () => Promise.resolve(pm);
		await m._getPictureIdForSequence("id");
		const res = await m._getPictureIdForSequence("id");
		expect(res).toBe("0005086d-65eb-4a90-9764-86b3661aaa77");
		expect(m._seqThumbPicId["id"]).toBe("0005086d-65eb-4a90-9764-86b3661aaa77");
	});

	it("awaits if a call is still running", async () => {
		const m = new Map(new Viewer());
		const pm = JSON.parse(fs.readFileSync(path.join(__dirname, "data", "Viewer_pictures_1.json"))).features[0];
		let called = 0;
		m.viewer._api.getExamplePictureMetadataForSequence = () => {
			called++;
			return new Promise(resolve => setTimeout(() => resolve(pm), 200));
		};
		m._getPictureIdForSequence("id");
		const res = await m._getPictureIdForSequence("id");
		expect(res).toBe("0005086d-65eb-4a90-9764-86b3661aaa77");
		expect(m._seqThumbPicId["id"]).toBe("0005086d-65eb-4a90-9764-86b3661aaa77");
		expect(called).toBe(1);
	});
});

describe("_getPictureMarker", () => {
	it("works", () => {
		const m = new Map(new Viewer());
		const res = m._getPictureMarker();
		expect(res).toBeDefined();
	});
});

describe("_listenToViewerEvents", () => {
	it("works", () => {
		const m = new Map(new Viewer());
		m._listenToViewerEvents();
		expect(m.viewer.addEventListener.mock.calls).toMatchSnapshot();
	});
});

describe("GeocoderParamsToURLString", () => {
	it("works", () => {
		const p = { bla: "blorg", you: 1 };
		const r = Map.GeocoderParamsToURLString(p);
		expect(r).toEqual("bla=blorg&you=1");
	});

	it("handles special characters", () => {
		const p = { bbox: "12,14,-45,78" };
		const r = Map.GeocoderParamsToURLString(p);
		expect(r).toEqual("bbox=12%2C14%2C-45%2C78");
	});

	it("filters nulls", () => {
		const p = { val1: undefined, val2: null, val3: 0 };
		const r = Map.GeocoderParamsToURLString(p);
		expect(r).toEqual("val3=0");
	});
});

describe("ForwardGeocodingNominatim", () => {
	it("works", () => {
		// Mock API search
		global.fetch = jest.fn(() => Promise.resolve({
			json: () => Promise.resolve(JSON.parse(fs.readFileSync(path.join(__dirname, "data", "Map_geocoder_nominatim.geojson"))))
		}));

		// Search config
		const cfg = { query: "bla", limit: 5, bbox: "17.7,-45.2,17.8,-45.1" };

		return Map.ForwardGeocodingNominatim(cfg).then(res => {
			expect(global.fetch.mock.calls).toEqual([["https://nominatim.openstreetmap.org/search?q=bla&limit=5&viewbox=17.7%2C-45.2%2C17.8%2C-45.1&format=geojson&polygon_geojson=1&addressdetails=1"]]);
			expect(res).toMatchSnapshot();
		});
	});
});

describe("ForwardGeocodingBAN", () => {
	it("works", () => {
		// Mock API search
		global.fetch = jest.fn(() => Promise.resolve({
			json: () => Promise.resolve(JSON.parse(fs.readFileSync(path.join(__dirname, "data", "Map_geocoder_ban.geojson"))))
		}));

		// Search config
		const cfg = { query: "bla", limit: 5, proximity: "17.7,-45.2" };

		return Map.ForwardGeocodingBAN(cfg).then(res => {
			expect(global.fetch.mock.calls).toEqual([["https://api-adresse.data.gouv.fr/search/?q=bla&limit=5&lat=17.7&lon=-45.2"]]);
			expect(res).toMatchSnapshot();
		});
	});
});
