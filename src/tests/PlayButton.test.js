import PlayButton from "../PlayButton";

function createButton() {
	const n = {
		children: [],
		container: {
			appendChild: () => {}
		},
		dispatchEvent: () => {},
		config: { lang: [] },
	};
	const btn = new PlayButton(n, true);
	btn.viewer = {
		addEventListener: jest.fn(),
		_geovisio: {
			isSequencePlaying: () => false,
			stopSequence: jest.fn(),
			playSequence: jest.fn()
		},
	};
	btn._bindViewerEvents();
	btn.toggleActive = jest.fn();
	return btn;
}

describe("constructor", () => {
	it("inits", () => {
		const btn = createButton();
		expect(btn).toBeDefined();
	});
});

describe("handleEvent", () => {
	it("updates when sequence starts playing", () => {
		const btn = createButton();
		btn.handleEvent({ type: "sequence-playing" });
		expect(btn.toggleActive.mock.calls).toEqual([[true]]);
	});

	it("updates when sequence stops playing", () => {
		const btn = createButton();
		btn.handleEvent({ type: "sequence-stopped" });
		expect(btn.toggleActive.mock.calls).toEqual([[false]]);
	});
});

describe("onClick", () => {
	it("handles when sequence is not playing", () => {
		const btn = createButton();
		btn.viewer._geovisio.isSequencePlaying = () => false;
		btn.onClick();
		expect(btn.viewer._geovisio.playSequence.mock.calls).toEqual([[]]);
		expect(btn.viewer._geovisio.stopSequence.mock.calls.length).toEqual(0);
	});

	it("handles when sequence is playing", () => {
		const btn = createButton();
		btn.viewer._geovisio.isSequencePlaying = () => true;
		btn.onClick();
		expect(btn.viewer._geovisio.playSequence.mock.calls.length).toEqual(0);
		expect(btn.viewer._geovisio.stopSequence.mock.calls).toEqual([[]]);
	});
});
