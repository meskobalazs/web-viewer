import MiniComponentButtons from "../MiniComponentButtons";
import Viewer from "../Viewer";

function createButton() {
	const d = document.createElement("div");
	d.classList.add("gvs", "gvs-has-map");

	const psv = document.createElement("div");
	psv.classList.add("gvs-psv");

	const map = document.createElement("div");
	map.classList.add("gvs-map");

	d.appendChild(psv);
	d.appendChild(map);

	const v = new Viewer(d);
	const m = new MiniComponentButtons(v, {
		"expand": "Expand",
		"expand_help": "Make the widget appear in full page",
		"show_psv": "Show the picture viewer",
		"show_map": "Show the map",
		"minimize": "Minimize",
		"minimize_help": "Hide this widget, you can show it again using button in bottom left corner",
	});
	return m;
}

jest.mock("../Viewer", () => (function (container) {
	return {
		container,
		isMapWide: () => false,
		addEventListener: jest.fn(),
		setUnfocusedVisible: jest.fn(),
		setFocus: jest.fn(),
		_api: {
			getDataBbox: () => Promise.resolve(),
			getPicturesTilesUrl: () => "http://localhost:5000/api/map/",
		},
	};
}));

describe("constructor", () => {
	it("inits", () => {
		const m = createButton();
		expect(m).toBeDefined();
	});
});

describe("_createButtonsDOM", () => {
	it("works", () => {
		const m = createButton();

		expect(m.container.tagName).toBe("DIV");
		expect(m.container.classList.contains("gvs-mini-buttons")).toBe(true);
	});

	it("minimizes small widget", () => {
		const m = createButton();
		m.btnMinimize.dispatchEvent(new Event("click"));
		expect(m._parent.setUnfocusedVisible.mock.calls).toEqual([[false]]);
	});

	it("set focus to map", () => {
		const m = createButton();
		m.btnExpand.dispatchEvent(new Event("click"));
		expect(m._parent.setFocus.mock.calls).toEqual([["map"]]);
	});

	it("set focus to photo viewer", () => {
		const m = createButton();
		m._parent.isMapWide = () => true;
		m.btnExpand.dispatchEvent(new Event("click"));
		expect(m._parent.setFocus.mock.calls).toEqual([["pic"]]);
	});

	it("shows minimized widget", () => {
		const m = createButton();
		m.badgeBtn.dispatchEvent(new Event("click"));
		expect(m._parent.setUnfocusedVisible.mock.calls).toEqual([[true]]);
	});
});
