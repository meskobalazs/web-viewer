import NextButton from "../NextButton";

function createButton() {
	const n = {
		children: [],
		container: { appendChild: () => {} },
		config: { lang: [] },
		_geovisio: { goToNextPicture: jest.fn() }
	};
	const btn = new NextButton(n);
	return btn;
}

describe("constructor", () => {
	it("inits", () => {
		const btn = createButton();
		expect(btn).toBeDefined();
	});
});

describe("onClick", () => {
	it("calls viewer goToNextPicture", () => {
		const btn = createButton();
		btn.onClick();
		expect(btn.viewer._geovisio.goToNextPicture.mock.calls).toEqual([[]]);
	});
});
