import PreviousButton from "../PreviousButton";

function createButton() {
	const n = {
		children: [],
		container: { appendChild: () => {} },
		config: { lang: [] },
		_geovisio: { goToPrevPicture: jest.fn() }
	};
	const btn = new PreviousButton(n);
	return btn;
}

describe("constructor", () => {
	it("inits", () => {
		const btn = createButton();
		expect(btn).toBeDefined();
	});
});

describe("onClick", () => {
	it("calls viewer goToPrevPicture", () => {
		const btn = createButton();
		btn.onClick();
		expect(btn.viewer._geovisio.goToPrevPicture.mock.calls).toEqual([[]]);
	});
});
