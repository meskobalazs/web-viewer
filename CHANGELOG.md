# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Before _2.0.0_, Viewer was embed in the [API repository](https://gitlab.com/geovisio/geovisio), this changelog only includes features since the split. Older details are present in [API Changelog](https://gitlab.com/geovisio/geovisio/-/blob/develop/CHANGELOG.md).

## [Unreleased]


## [2.1.0] - 2023-07-20

Empty release to follow the API's minor version

## [2.0.7] - 2023-07-10

### Added
- A new function `reloadVectorTiles` allows users to force full refresh of GeoVisio/STAC API tiles, for example to reflect backend changes done on sequences or pictures data.

### Changed
- Map is updated sooner to be more in sync when loading incoming picture

### Fixed
- Thumbnail of hidden sequences where not correctly shown


## [2.0.6] - 2023-06-30

### Added
- A `fetchOptions` parameter for Viewer allows to embed custom settings for internal `fetch` calls, for example to enable authenticated calls against API
- Hidden sequences or pictures for an user are shown on map if user is authenticated (rendered in a gray tone)
- A loader screen is shown while API, map and first picture loads. It also displays an user-friendly error message if necessary.

### Changed
- Transitions between flat pictures are smoother
- Arrows are more visible in zoom 0 to 70% for classic pictures


## [2.0.5] - 2023-06-09

### Changed
- Transition between pictures is lowered to 500ms

### Fixed
- Viewer was not showing vector tiles anymore for GeoVisio API <= 2.0.1


## [2.0.4] - 2023-06-08

### Changed
- Read sequence info from vector tiles if available

## [2.0.3] - 2023-06-08

### Changed
- Viewer calls API based on its advertised capabilities in its landing page, so many API calls are faster. This is particularly useful for __tiles URL__, which can be now determined based on API response, making it not necessary to define it on Viewer settings.
- Viewer `endpoint` parameter should now be a direct link to API landing page, instead of its search endpoint.

### Fixed
- Default image shown at start is better oriented


## [2.0.2] - 2023-06-05

### Added
- A download button is shown to get the HD picture currently shown by viewer.
- Version and git commit are shown at viewer start-up in web browser console.

### Changed
- Map maximum zoom is set by default to 24, but still can be changed through Viewer `options.map.maxZoom` parameter.
- Error message from viewer doesn't block using navbar


## [2.0.1] - 2023-05-26

### Added
- Support of translations of viewer labels. This is defined based on user browser preferences, or a `lang` setting that can be passed to Viewer. Available languages are English (`en`) and French (`fr`).

### Changed
- Updated Photo Sphere Viewer to 1.5.0, solves some low-quality display issues

### Fixed
- Label shown on small action button (bottom left corner) was showing [wrong label when map was hidden](https://gitlab.com/geovisio/web-viewer/-/issues/16)


## [2.0.0] - 2023-04-27

### Added

- Publish a `geovisio@develop` viewer version on NPM for each commit on the `develop` branch. You can depend on this version if you like to live on the edge.
- Viewer displays the name of picture author, next to the picture date.
- Viewer offers a search bar to easily find a street or city on map. It can use either _Nominatim_ or _Base Adresse Nationale_ geocoders, as well as your own geocoder engine.
- A download button allows to download full-resolution image directly

### Changed

- Viewer dependencies have been updated :
  - Photo Sphere Viewer 5.1 (inducing __breaking changes__)
  - MapLibre 2.4
  - JS Library Boilerplate 2.7
- Viewer unit tests are covering a broader part of functionalities. To make this possible, Photo Sphere Viewer is differently associated to GeoVisio viewer (as a property `this.psv` instead of GeoVisio inheriting directly from PSV). Original PSV object is accessible through `getPhotoViewer` function.
- Linked to previous point, map and photo viewer are two distinct DOM elements in a main GeoVisio container. Many CSS classes have been changed, and `setMapWide` JS function renamed into `setFocus`.
- Improved rendering of flat, vertical images in viewer and thumbnail.
- Improved error messages for missing parameters in the Viewer initialization
- Viewer has a better rendering on small-screen devices

### Fixed

- Map couldn't be disabled due to an error with URL `hash` handling
- MapLibre was raising issues on built versions on NPM
- [A Photo Sphere Viewer under Chrome issue](https://github.com/mistic100/Photo-Sphere-Viewer/issues/937) was making classic pictures not visible


[Unreleased]: https://gitlab.com/geovisio/web-viewer/-/compare/2.1.0...develop
[2.1.0]: https://gitlab.com/geovisio/web-viewer/-/compare/2.0.7...2.1.0
[2.0.7]: https://gitlab.com/geovisio/web-viewer/-/compare/2.0.6...2.0.7
[2.0.6]: https://gitlab.com/geovisio/web-viewer/-/compare/2.0.5...2.0.6
[2.0.5]: https://gitlab.com/geovisio/web-viewer/-/compare/2.0.4...2.0.5
[2.0.4]: https://gitlab.com/geovisio/web-viewer/-/compare/2.0.3...2.0.4
[2.0.3]: https://gitlab.com/geovisio/web-viewer/-/compare/2.0.2...2.0.3
[2.0.2]: https://gitlab.com/geovisio/web-viewer/-/compare/2.0.1...2.0.2
[2.0.1]: https://gitlab.com/geovisio/web-viewer/-/compare/2.0.0...2.0.1
[2.0.0]: https://gitlab.com/geovisio/web-viewer/-/compare/1.5.0...2.0.0
